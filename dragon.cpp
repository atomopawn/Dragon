#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;

constexpr int WIDTH = 30;
constexpr int HEIGHT = 30;

/* Clear the map by placing a dot in each grid square.  
 *
 * Inputs: a HEIGHT x WIDTH map array
 * Output: none
 *
 * Side Effects: Clears the map
*/
void initMap(char map[][WIDTH])
{
	for (int j = 0; j < HEIGHT; ++j) {
		for (int i = 0; i < WIDTH; ++i) {
			map[j][i] = '.';
		}
	}
}

/* Draw the map to the screen using cout 
 *
 * Inputs: a HEIGHT x WIDTH map array
 * Output: none
 *
 * Side Effects: Prints the map to the screen 
*/
void drawMap(char map[][WIDTH])
{

}

/* Place a character on the map at a random location
 *
 * Inputs: a HEIGHT x WIDTH map array
 *         a symbol representing the "object" to place
 *
 * Outputs: The X coordinate and Y coordinate of the placed object.
 *
 * Side Effects: Modifies the "map" array by placing an object in an empty cell.
*/
void placeObject(char map[][WIDTH], int& obX, int& obY, char symbol)
{
	//Find an empty cell of the grid
	do {
		//Pick a Random Number between 0 and WIDTH-1.  Better than random() % WIDTH because it uses more entropy bits.
		obX = int( random() * 1.0 * WIDTH / RAND_MAX);
		
		//Pick a Random Number between 0 and HEIGHT-1.  
		obY = int( random() * 1.0 * HEIGHT / RAND_MAX);

	} while (map[obX][obY] != '.');

	map[obY][obX] = symbol;
}

/* Move the dragon in a random direction
 *
 * Inputs: a HEIGHT x WIDTH map array
 * 
 * Input/Output parameters: the X and Y coordinates of the dragon
*/

void moveDragon(char map[][WIDTH], int& dragX, int& dragY)
{
	//Pick a number between 0 and 3: 0 = North, 1 = East, 2 = South, 3 = West
	int dir = int ( random() * 4 / RAND_MAX );

	if (dir == 0) {
		--dragY; //Move North(Up)
	}
	else if (dir == 1) {
		++dragX; //Move East(Right)
	}
	else if (dir == 2) {
		++dragY; //Move South(Down)
	}
	else if (dir == 3) {
		--dragX; //Move West(Left)
	}
}

/* Check if the player has been eaten by the dragon
 *
 * Inputs: X and Y coordinates for the player, X and Y coordinates for the dragon
 *
 * Outputs: True if the coordinates match, false otherwise
*/

bool eaten(const int playerX, const int playerY, const int dragX, const int dragY)
{
	return (playerX == dragX) && (playerY == dragY);
}

/* The main game code */
int main(int argc, char* argv[])
{
	char grid[HEIGHT][WIDTH];
	int playerX;
	int playerY;
	int dragX;
	int dragY;
	int treasX;
	int treasY;
	char dir = '.';
	int score = 0;

	srandom(30); //Set "initial seed" for the random number generator -- this will make the motions of the dragons predictable/testable
	
	//Initialize the map to all dots
	initMap(grid);

	//Put the Dragon on the map at a random location
	//Store the coordinates in "dragX" and "dragY"
	placeObject(grid, dragX, dragY, '@');

	//Put the first treasure on the map at a random location
	//Store the coordinates in "treasX" and "treasY"
	placeObject(grid, treasX, treasY, '$');

	//Put the Player on the map at a random location
	placeObject(grid, playerX, playerY, 'p');

	//The main game loop
	//   Until eaten by a dragon . . .
	while (!eaten(playerX, playerY, dragX, dragY)) {
		//Draw the map
		drawMap(grid);

		//Ask the user which way to go
		cout << "Which direction? (N: North, S: South, E: East, W: West)" << endl;
		cin >> dir;

		//Erase the player from his (or her) old position
		grid[playerY][playerX] = '.';

		//Update the player's position
		switch (dir) {
			case 'N':
				--playerY;
				break;

			case 'S':
				++playerY;
				break;
			default:
				cout << "You stand still." << endl;
		}

		//Draw the player at his(or her) new position
		grid[playerY][playerX] = 'p';

		//Erase the dragon
		grid[dragY][dragX] = '.';

		//Move the dragon
		moveDragon(grid, dragX, dragY);

		//Draw the dragon at the new position
		grid[dragY][dragX] = '@';

		//Check to see if we got the treasure
		if (playerX == treasX && playerY == treasY) {
			cout << "You found a buried chest!  Inside you find a coin!" << endl;
			++score;

			//Place the next treasure on the map at a random location
			//Store the coordinates in treasX and treasY
			placeObject(grid, treasX, treasY, '$');
		}
	}

	//When the main game loop ends, we know the dragon got us
	cout << "You have been eaten by a dragon!  Game Over." << endl;

	cout << "However, you found " << score 
	     << "coins before your untimely demise." << endl; 

}
