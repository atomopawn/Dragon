#include "maze.h"
#include <fstream>
#include <sstream>

using namespace std;

/* Initialize a new maze object from a file */
Maze::Maze(const std::string filename)
{
	ifstream fin(filename);
	fin >> m_width >> m_height;

	string line;
	int row = 0;
	while (	getline(fin, line) )
	{
		istringstream strin(line);
		vector <char> map_row;
		for (int col = 0; col < m_width; ++col) {
			char ch;
			strin >> ch;
			map_row.push_back(ch);	
		}
		m_grid.push_back(map_row);
		++row;
	}
}

int Maze::width() const
{
	return m_width;
}

int Maze::height() const
{
	return m_height;
}

/* Print the map to the screen using "cout" */

void Maze::drawMap() const
{

}

/* Place an object, represented by "symbol" into m_grid at random coordinates.
 * Store the coordinates in the output parameters x and y. */

void Maze::placeObject(int& x, int& y, const char symbol)
{

}

/* Erase an object at position x, y by drawing a "." character into m_grid at
 * the corresponding coordinates. */

void Maze::erase(const int x, const int y)
{

}

/* Place an object, represented by "symbol" into m_grid at coordinates x, y. */

void Maze::place(const int x, const int y, const char symbol)
{

}
