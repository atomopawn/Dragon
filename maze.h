#ifndef MAZE_H
#define MAZE_H

#include <vector>
#include <string>

class Maze
{
	private:
		int m_width;
		int m_height;
		std::vector<std::vector<char>> m_grid;

	public:
		Maze(const std::string filename);

		void drawMap() const;

		void placeObject(int& x, int& y, const char symbol);
		void erase(const int x, const int y);
		void place(const int x, const int y, const char symbol);

		int width() const;
		int height() const;

};
#endif //MAZE_H
